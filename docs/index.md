# 최고의 비동기 설정: FastAPI, SQLModel, Alembic, Pytest <sup>[1](#footnote_1)</sup>

FastAPI는 요즘 인기 있는 주제이며 이 프레임워크를 사용하여 비동기 웹 서버를 설정하는 방법을 공유하려고 한다. 다음은 이 포스팅에서 사용하는 Python 패키지에 대한 (전체 그림을 그리기 위한) 간략한 설명이다.

[**Poetry**](https://python-poetry.org) - Python에서 의존성 관리와 패키징을 위한 도구이다. 프로젝트가 의존하는 라이브러리를 선언하고 이를 관리(설치/업데이트)할 수 있다.

[**FastAPI**](https://fastapi.tiangolo.com) - 표준 Python 타입 힌트를 기반으로 Python 3.6+로 API를 구축하기 위한 현대적이고 빠른(고성능) 웹 프레임워크이다.

[**Pydantic**](https://pydantic-docs.helpmanual.io) - Python 타입 힌트를 사용한 데이터 유효성 검사와 설정 관리.

[**SQLAlchemy**](https://www.sqlalchemy.org) - 어플리케이션 개발자에게 SQL의 모든 기능과 유연성을 제공하는 Python SQL 툴킷과 객체 관계형 매퍼이다.

[**SQLModel**](https://sqlmodel.tiangolo.com) - SQLModel은 Python 객체를 사용하여 Python 코드에서 SQL 데이터베이스와 상호 작용하기 위한 라이브러리이다.

[**Alembic**](https://alembic.sqlalchemy.org/en/latest/) - Alembic은 SQLAlchemy 데이터베이스 툴킷과 함께 사용하기 위한 Python용 경량 데이터베이스 마이그레이션 도구이다.

<a name="footnote_1">1</a>: 이 페이지는 [The ultimate async setup: FastAPI, SQLModel, Alembic, Pytest](https://medium.com/@estretyakov/the-ultimate-async-setup-fastapi-sqlmodel-alembic-pytest-ae5cdcfed3d4)를 편역하였다.
