# 최고의 비동기 설정: FastAPI, SQLModel, Alembic, Pytest

## 프로젝트 구조

![](./1_Qf175q4bTIoI3e7diolpHg.webp)

이 포스팅에서 비동기 설정의 예로 간단한 어플리케이션을 사용하고 있다. 이 어플리케이션에는 영웅의 기록을 저장하는 테이블이 하나만 있다. 웹 서비스 자체에는 데이터베이스에서 레코드를 생성, 가져오기, 패치 및 삭제하는 기능이 있다.

## 개발 환경
개발 환경을 설정하는 데 시간을 낭비하고 싶지 않다면 다음 섹션으로 넘어가세요.

[Poetry 웹 페이지](https://python-poetry.org/docs/)로 이동하여 플랫폼에 적합한 설치 방법을 확인하자. 새 프로젝트를 생성하려면 이 기본 [Poetry 사용 가이드](https://pythonnecosystems.gitlab.io/python-poetry)를 사용하세요.

### Poetry 설치

```bash
$ curl -sSL https://install.python-poetry.org | python3 <설치할 디렉토리>
```

위의 `<설치할 디렉토리>`는 사용자의 홈 디렉토리로 하면 무난하다.

설치를 확인한다.

```python
$ poetry --version
Poetry (version 1.7.1)
```

`my-project`로 새로운 Python 프로젝트를 시작한다.

```bash
# with poetry
$ poetry new my-project
```

Poetry 초기화는 누구나 이해할 수 있는 친숙한 대화 상자를 제공한다. 여기서 한 가지 언급하고 싶은 것은 import문을 정렬하고 코드 스타일을 확인하기 위해 poetry 개발 환경에 `isort`와 `flake8` 패키지를 추가한다는 점이다.

프로젝트에 중요한 패키지를 설치해 보겠다(초기화 중에 설치하지 않았다면).

```bash
$ poetry add fastapi
$ poetry add sqlmodel
$ poetry add dotenv
$ poetry add uvicorn
```

이 단계가 끝나면 프로젝트의 루트에 두 개의 파일, 즉 p`yproject.toml`과 `poetry.lock`이 생성되어야 한다. 일반적으로 이 두 파일은 프로젝트 종속성을 설명한다.

## 데이터베이스
요즘 가장 인기 있는 DBMS 중 하나인 PostgreSQL 데이터베이스를 설정에 사용한다. 데이터베이스를 설치하려면 도커 컨테이너를 사용하는 것이 좋다. 다음은 비트나미 이미지를 사용하는 간단한 명령어이다.

```bash
docker run --name heroes-pg -d -e POSTGRESQL_PASSWORD=thepass123 -p 5432:5432 bitnami/postgresql:13
```

Docker에 대해 전혀 모르는 경우 링크를 방문하여 몇 가지 지식과 설치 방법을 확인하세요. 위의 명령은 "heroes-pg"라는 이름의 PostgreSQL 컨테이너를 설치하고 localhost에서 5432 포트를 열며, 데이터베이스의 비밀번호는 "thepass123", 사용자 이름은 기본적으로 "postgres"입니다. 편의를 위해 [pgadmin](https://www.pgadmin.org/), [dbeaver](https://dbeaver.io/), [DataGrip](https://www.jetbrains.com/datagrip/) 또는 기타 도구를 사용하여 데이터베이스와 상호 작용할 수 있다. 다음은 별도의 데이터베이스를 만들고 앱의 역할을 만드는 데 사용하는 스크립트이다.

```sql
CREATE USER hero WITH PASSWORD 'heroPass123';
CREATE DATABASE heroes_db OWNER hero;
```

이 경우 앱의 데이터베이스 연결 문자열은 다음과 같이 표시된다.

```
postgresql+asyncpg://hero:heroPass123@0.0.0.0:5432/
```

## 환경 변수
프로젝트의 환경 변수를 설정하기 위해 Pydantic 접근 방식을 사용하겠다. 프로젝트의 루트 아래에 `.env` 파일을 만든다.

```
# BASE
API_V1_PREFIX="/api/v1"
DEBUG=True
PROJECT_NAME="Heroes App (local)"
VERSION="0.1.0"
DESCRIPTION="The API for Heroes app."
# DATABASE
DB_ASYNC_CONNECTION_STR="postgresql+asyncpg://hero:heroPass123@0.0.0.0:5432/heroes_db"
```

이러한 변수를 읽기 위해 설정을 선언하는 `config.py`(`app/core/config.py`)를 만든다. `config.py`의 내용은 다음과 같다.

```python
# app/core/config.py

from pydantic import BaseSettings


class Settings(BaseSettings):
   # Base
   api_v1_prefix: str
   debug: bool
   project_name: str
   version: str
   description: str

   # Database
   db_async_connection_str: str
```

파일 로딩은 어플리케이션 초기화 시 이루어져야 하므로 `__init__.py`(`app/__init__.py`)에 다음과 같은 코드를 작성한다.

```python
# app/__init__.py

from os import getenv

from dotenv import load_dotenv

from app.core.config import Settings

load_dotenv(getenv("ENV_FILE"))

settings = Settings()
```

여기서는 `python-dotenv` 패키지를 사용하여 `.env` 파일을 로드하고 나중에 Dockerfile 명령에서 다른 환경 파일을 설정할 수 있다.

`main.py`(`app/main.py`) 파일에서 환경 변수 중 일부 환경 변수를 테스트해 보겠다.

```python
# app/main.py

import uvicorn
from fastapi import FastAPI

from app import settings
from app.core.models import HealthCheck

app = FastAPI(
   title=settings.project_name,
   version=settings.version,
   openapi_url=f"{settings.api_v1_prefix}/openapi.json",
   debug=settings.debug
)


@app.get("/", response_model=HealthCheck, tags=["status"])
async def health_check():
   return {
       "name": settings.project_name,
       "version": settings.version,
       "description": settings.description
   }


if __name__ == '__main__':
   uvicorn.run("main:app", port=8080, host="0.0.0.0", reload=True)
```

여기서는 스키마 섹션 아래의 SwaggerUI에서 멋지게 표현하기 위해 `models.py`(`app/core/models.py`) 파일의 `HealthCheck` 모델을 사용한다.

```python
# app/core/models.py

from pydantic import BaseModel


class HealthCheck(BaseModel):
   name: str
   version: str
   description: str
```

다음 명령을 사용하여 서비스를 시작한다.

```bash
$ uvicorn app.main:app
```

URL http://0.0.0.0:8000을 사용하여 서비스 health check에 액세스할 수 있어야 하며, 콘텐츠는 다음과 같이 표시된다.

```
{
   "name":"Heroes App (local)",
   "version":"0.1.0",
   "description":"The API for Heroes app."
}
```

이 응답의 JSON 데이터는 `.env` 파일이 제공하는 데이터를 반영한다.

## SQLAlchemy
데이터베이스에 연결할 수 있는 SQLAlchemy 엔진을 만들겠다. 비동기 엔진의 선언은 `db.py`(`app/core/db.py`) 파일에 있다.

```python
# app/core/db.py

from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlmodel.ext.asyncio.session import AsyncSession

from app import settings

async_engine = create_async_engine(
   settings.db_async_connection_str,
   echo=True,
   future=True
)


async def get_async_session() -> AsyncSession:
   async_session = sessionmaker(
       bind=async_engine, class_=AsyncSession, expire_on_commit=False
   )
   async with async_session() as session:
       yield session
```

asyncpg 드라이버를 사용하므로 `poetry add asyncpg`로 설치하는 것을 잊지 마세요. 나중에 데이터베이스 세션을 핸들러에 전달하려면 다음과 같이 [Depends](https://pythonnecosystems.gitlab.io/fastapi/dependencies/) 접근 방식을 사용할 수 있다.

```
connection: AsyncSession = Depends(get_async_session)
```

## SQLModel
많은 모델을 만들면 "uuid", "created_at", "updated_at"와 같은 공통 필드가 있게 된다. 따라서 앞으로 사용할 관련 모델을 별도의 파일로 만드는 것이 좋습니다. 다음은 `models.py`(`app/core/models.py`) 파일에서 수행하는 방법이다.

```python
# app/core/models.py

import uuid as uuid_pkg
from datetime import datetime

from pydantic import BaseModel
from sqlalchemy import text
from sqlmodel import Field, SQLModel


class HealthCheck(BaseModel):
   name: str
   version: str
   description: str


class UUIDModel(SQLModel):
   uuid: uuid_pkg.UUID = Field(
       default_factory=uuid_pkg.uuid4,
       primary_key=True,
       index=True,
       nullable=False,
       sa_column_kwargs={
           "server_default": text("gen_random_uuid()"),
           "unique": True
       }
   )


class TimestampModel(SQLModel):
   created_at: datetime = Field(
       default_factory=datetime.utcnow,
       nullable=False,
       sa_column_kwargs={
           "server_default": text("current_timestamp(0)")
       }
   )

   updated_at: datetime = Field(
       default_factory=datetime.utcnow,
       nullable=False,
       sa_column_kwargs={
           "server_default": text("current_timestamp(0)"),
           "onupdate": text("current_timestamp(0)")
       }
   )
```

이제 `UUIDModel`과 `TimestampModel` 모델이 생겼으니 첫 번째 어플리케이션 모델을 만들 준비가 되었다. 실무에서는 기본 필드와 "비공개" 필드를 구분한다. 기본 필드는 데이터를 만들고 읽을 때 모두 사용되며, 외래 키와 ID와 같은 "비공개" 필드는 기본 모델 아래에 배치한다. 모델과 관련 기능은 별도의 서브모듈에 배치하므로 `hero model.py`(`app/heroes/models.py`) 파일은 다음과 같다.

```python
# app/heros/models.py

from typing import Optional

from sqlalchemy import Column, event
from sqlalchemy.databases import postgres
from sqlmodel import Field, SQLModel

from app.core.models import TimestampModel, UUIDModel

prefix = "hrs"


hrs_role_type = postgres.ENUM(
   "mage",
   "assassin",
   "warrior",
   "priest",
   "tank",
   name=f"{prefix}_role"
)


@event.listens_for(SQLModel.metadata, "before_create")
def _create_enums(metadata, conn, **kw):  # noqa: indirect usage
   hrs_role_type.create(conn, checkfirst=True)


class HeroBase(SQLModel):
   nickname: str = Field(max_length=255, nullable=False)
   role: Optional[str] = Field(
       sa_column=Column(
           "role",
           hrs_role_type,
           nullable=True
       )
   )


class Hero(
   TimestampModel,
   HeroBase,
   UUIDModel,
   table=True
):
   __tablename__ = f"{prefix}_heroes"


class HeroRead(HeroBase, UUIDModel):
   ...


class HeroCreate(HeroBase):
   ...


class HeroPatch(HeroBase):
   nickname: Optional[str] = Field(max_length=255)

```

이 예에서는 열거형 필드를 처리하는 방법도 보이고 있다. 다음 코드는 향후 테스트에 도움이 될 것입니다. 

```python
@event.listens_for(SQLModel.metadata, "before_create")
def _create_enums(metadata, conn, **kw):  # noqa: indirect usage
   hrs_role_type.create(conn, checkfirst=True)
```

테스트 환경은 데이터베이스 테이블을 생성하고 드롭할 것이며 `ENUM` 생성을 위한 리스너를 제공해야 한다. Hero의 모델을 나타내는 테이블을 만들려면 alembic 패키지를 사용해야 한다.

## Alemic
`alembic`을 사용하려면 패키지를 설치해야 한다. 

```bash
$ poetry add alembic
``` 

모든 마이그레이션을 `migrations` 디렉토리에 보관하는 것을 선호하므로, alembic 설정을 초기화해 보겠다. 

```bash
$ alembic init -t async migrations
```

이 명령은 구성 파일이 있는 `migrations` 디렉터리를 생성하며 여기서 몇 가지 변경을 수행한다.

테이블과 기타 데이터베이스 엔티티에 대해 멋진 명명 규칙을 사용하는 것을 보통 선호하며, 마이그레이션 자동 생성을 위한 이 코드를 `env.py`(`migrations/env.py`) 파일에 추가하는 것을 추천한다.

```python
# migrations/env.py

import asyncio
import os
from json import loads
from logging.config import fileConfig

from alembic import context
from sqlalchemy import engine_from_config, pool
from sqlalchemy.ext.asyncio import AsyncEngine
from sqlmodel import SQLModel

config = context.config

if config.config_file_name is not None:
   fileConfig(config.config_file_name)

target_metadata = SQLModel.metadata

target_metadata.naming_convention = {
   "ix": "ix_%(column_0_label)s",
   "uq": "uq_%(table_name)s_%(column_0_name)s",
   "ck": "ck_%(table_name)s_%(constraint_name)s",
   "fk": "fk_%(table_name)s_%(column_0_name)"
         "s_%(referred_table_name)s",
   "pk": "pk_%(table_name)s"
}
```

Alembic이 모델을 인식하도록 하려면 다음과 같이 `env.py` 파일에서 모델을 임포트해야 한다.

```python
from app.heroes.models import Hero  # noqa: 'autogenerate' support
```

경우에 따라 셀러리와 같은 다른 어플리케이션이나 postgis와 같은 일부 데이터베이스 확장 프로그램에서 만든 테이블을 무시해야 할 수도 있다. 따라서 이 코드를 추가하여 `.env` 파일에 언급된 테이블 목록을 무시할 수 있도록 한다.

```python
exclude_tables = loads(os.getenv("DB_EXCLUDE_TABLES"))


def filter_db_objects(
       object,  # noqa: indirect usage
       name,
       type_,
       *args,  # noqa: indirect usage
       **kwargs  # noqa: indirect usage
):
   if type_ == "table":
       return name not in exclude_tables

   if type_ == "index" and name.startswith("idx") and name.endswith("geom"):
       return False

   return True


def run_migrations_offline():
   url = os.getenv("DB_ASYNC_CONNECTION_STR")
   context.configure(
       url=url,
       target_metadata=target_metadata,
       literal_binds=True,
       dialect_opts={"paramstyle": "named"},
       include_object=filter_db_objects
   )

   with context.begin_transaction():
       context.run_migrations()


def do_run_migrations(connection):
   context.configure(connection=connection, target_metadata=target_metadata)

   with context.begin_transaction():
       context.configure(
           connection=connection,
           target_metadata=target_metadata,
           include_object=filter_db_objects
       )
       context.run_migrations()


async def run_migrations_online():
   config_section = config.get_section(config.config_ini_section)
   url = os.getenv("DB_ASYNC_CONNECTION_STR")
   config_section["sqlalchemy.url"] = url

   connectable = AsyncEngine(
       engine_from_config(
           config_section,
           prefix="sqlalchemy.",
           poolclass=pool.NullPool,
           future=True,
       )
   )

   async with connectable.connect() as connection:
       await connection.run_sync(do_run_migrations)

   await connectable.dispose()


if context.is_offline_mode():
   run_migrations_offline()
else:
   asyncio.run(run_migrations_online())
```

또한 `.env` 파일에서 데이터베이스 연결 문자열을 가져온다는 점에 유의하세요.

또한 모든 마이그레이션 파일에서 가져온 모듈에 SQLModel을 표시할 수 있도록 `script.py.mako`(`migrations/script.py.mako`) 파일을 수정해야 한다. 

다음 코드 바로 앞에

```python
“${imports if imports else “”}
```

아래 줄을 추가하세요.

```python
import sqlmodel
```

이제 첫 번째 마이그레이션을 할 준비가 되었다. 다음 명령을 사용하여 마이그레이션/버젼 디렉터리에 마이그레이션 파일을 생성한다. 
```bash
$ alembic revision --autogenerate -m "heroes"
```

저자 경우에는 생성된 마이그레이션 파일 이름이 "`886322ad66ff_heroes`"였으므로 "`0001_886322ad66ff_heroes`"와 같이 이름을 바꾸는 것이 좋다. 파일 이름을 변경해도 성능에는 영향을 미치지 않지만 진화 타임라인에서 마이그레이션을 식별하는 데 도움이 된다.

명령을 실행하여 데이터베이스에 변경 사항을 적용한다. 모든 것이 정상이면 추가 DDL을 통해 데이터베이스에 `hrs_heroes` 테이블이 생성된다.

```sql
create table if not exists hrs_heroes
(
   role       hrs_role,
   uuid       uuid      default gen_random_uuid()    not null
   constraint pk_hrs_heroes
   primary key,
   nickname   varchar(255)                           not null,
   created_at timestamp default CURRENT_TIMESTAMP(0) not null,
   updated_at timestamp default CURRENT_TIMESTAMP(0) not null
);
alter table hrs_heroes
owner to hero;
create unique index if not exists ix_hrs_heroes_uuid
on hrs_heroes (uuid);
```

마이그레이션을 다운그레이드하려면 마이그레이션 파일의 다운그레이드 함수에 `op.execute("DROP TYPE hrs_role;")` 줄을 추가하는 것을 잊지 마세요.

## CRUD
CRUD는 *생성*, *읽기*, *업데이트*, *삭제* 기능을 의미하며, 이 기능은 `crud.py`(`app/heros/crud.py`) 파일에서 구현할 예정이다.

```python
# app/heros/crud.py

from uuid import UUID

from fastapi import HTTPException
from fastapi import status as http_status
from sqlalchemy import delete, select
from sqlmodel.ext.asyncio.session import AsyncSession

from app.heroes.models import Hero, HeroCreate, HeroPatch


class HeroesCRUD:
   def __init__(self, session: AsyncSession):
       self.session = session
```

여기에서 `HeroesCRUD` 클래스에 대한 모든 임포트와 `__init__` 함수를 볼 수 있다. 비동기 기능을 하나씩 구현해 보겠다.

```python
  async def create(self, data: HeroCreate) -> Hero:
     values = data.dict()

     hero = Hero(**values)
     self.session.add(hero)
     await self.session.commit()
     await self.session.refresh(hero)

     return hero
```

데이터를 제공하고 생성된 `Hero` 객체를 검색하는 간단한 함수이다. 커밋을 수행하는 것을 잊지 마세요. 그렇지 않으면 데이터베이스에 대한 변경 사항이 저장되지 않는다.

```python
  async def get(self, hero_id: str | UUID) -> Hero:
     statement = select(
         Hero
     ).where(
         Hero.uuid == hero_id
     )
     results = await self.session.execute(statement=statement)
     hero = results.scalar_one_or_none()  # type: Hero | None

     if hero is None:
         raise HTTPException(
             status_code=http_status.HTTP_404_NOT_FOUND,
             detail="The hero hasn't been found!"
         )

     return hero
```

ID로 객체를 검색하기 위해 `get` 메서드를 구현했다. 여기서는 데이터베이스와 통신에서 무슨 일이 일어나고 있는지 이해하기에 매우 편한 select SQL 빌더를 사용했다.

```python
  async def patch(self, hero_id: str | UUID, data: HeroPatch) -> Hero:
     hero = await self.get(hero_id=hero_id)
     values = data.dict(exclude_unset=True)

     for k, v in values.items():
         setattr(hero, k, v)

     self.session.add(hero)
     await self.session.commit()
     await self.session.refresh(hero)

     return hero
```

레코드 패치는 객체 속성을 업데이트하여 수행되며, `get` 메서드와 동일한 기능을 sqlalchemy의 update SQL 빌더 함수를 사용하여 수행할 수 있다.

```python

  async def delete(self, hero_id: str | UUID) -> bool:
     statement = delete(
         Hero
     ).where(
         Hero.uuid == hero_id
     )

     await self.session.execute(statement=statement)
     await self.session.commit()

     return True
```

객체를 삭제하려면 위 코드에서 만든 것처럼 SQL 빌더를 사용하거나 다음과 같이 비동기 세션 메서드 `delete`를 사용할 수 있다

```python
await self.session.delete(hero)
```

여기서 CRUD 기능을 끝내고 API 처리기로 넘어가겠다.

## API 처리기
핸들러 함수에 CRUS 기능을 제공할 의존성을 만들어야 한다. `dependencies.py`(`app/heros/dependencies.py`) 파일 아래에 만들겠다.

```python
# app/heros/dependencies.py

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.db import get_async_session
from app.heroes.crud import HeroesCRUD


async def get_heroes_crud(
       session: AsyncSession = Depends(get_async_session)
) -> HeroesCRUD:
   return HeroesCRUD(session=session)
```

이제 데이터베이스에 Heroes을 생성할 첫 번째 처리기를 만들 준비가 되었다.

```python
# app/heros/api.py

from fastapi import APIRouter, Depends
from fastapi import status as http_status

from app.heroes.crud import HeroesCRUD
from app.heroes.dependencies import get_heroes_crud
from app.heroes.models import HeroCreate, HeroRead

router = APIRouter()


@router.post(
   "",
   response_model=HeroRead,
   status_code=http_status.HTTP_201_CREATED
)
async def create_hero(
       data: HeroCreate,
       heroes: HeroesCRUD = Depends(get_heroes_crud)
):
   hero = await heroes.create(data=data)

   return hero
```

요청에 대한 응답은 `response_model` 인수 아래에 pydantic 모델을 표시했듯이 클라이언트에 JSON 형식의 HeroRead 모델 데이터를 제공한다.

```python
# app/heros/api.py

@router.get(
   "/{hero_id}",
   response_model=HeroRead,
   status_code=http_status.HTTP_200_OK
)
async def get_hero_by_uuid(
       hero_id: str,
       heroes: HeroesCRUD = Depends(get_heroes_crud)
):
   hero = await heroes.get(hero_id=hero_id)

   return hero
```

데이터베이스에서 레코드를 검색하는 HeroesCRUD `get` 메서드에 URL 경로 매개변수 `hero_id`를 사용하여 `uuid`를 제공한다.

```python
# app/heros/api.py

@router.patch(
   "/{hero_id}",
   response_model=HeroRead,
   status_code=http_status.HTTP_200_OK
)
async def patch_hero_by_uuid(
       hero_id: str,
       data: HeroPatch,
       heroes: HeroesCRUD = Depends(get_heroes_crud)
):
   hero = await heroes.patch(hero_id=hero_id, data=data)

   return hero
```

delete 처리기의 경우 `StatusMessage`라고 부르는 또 다른 응답 모델이 필요하다. 이 모델은 많은 경우에 일반적으로 사용되는 모델이므로 `models.py`(`app/core/models.py`) 파일 아래에 배치한다.

```python
class StatusMessage(BaseModel):
   status: bool
   message: str
```

마지막 처리기는 다음과 같다.

```python
# app/heros/api.py

@router.delete(
   "/{hero_id}",
   response_model=StatusMessage,
   status_code=http_status.HTTP_200_OK
)
async def delete_hero_by_uuid(
       hero_id: str,
       heroes: HeroesCRUD = Depends(get_heroes_crud)
):
   status = await heroes.delete(hero_id=hero_id)

   return {"status": status, "message": "The hero has been deleted!"}
```

`app/core/models.py`에서 `StatusMessage` 모델을 임포트하는 것을 잊지 마세요. 이제 라우터를 메인 애플리케이션에 연결할 준비가 되었다.

여기서는 `endpoints.py`(`app/router/api_v1/endpoints.py`) 파일을 사용하여 핸들러를 기본 앱에 연결한다.

```python
# app/router/api_v1/endpoints.py

from fastapi import APIRouter

from app.heroes.api import router as heroes_router

api_router = APIRouter()

include_api = api_router.include_router

routers = (
   (heroes_router, "heroes", "heroes"),
)

for router_item in routers:
   router, prefix, tag = router_item

   if tag:
       include_api(router, prefix=f"/{prefix}", tags=[tag])
   else:
       include_api(router, prefix=f"/{prefix}")
```

마지막으로 `app/main.py` 파일에 라우터를 메인 라우터에 포함시키는 것이다.

```python
from app.router.api_v1.endpoints import api_router

...

app.include_router(api_router, prefix=settings.api_v1_prefix)
```

이제 서버를 시작하고 http://0.0.0.0:8000/docs를 확인한다. SwaggerUI 인터페이스가 출력되고, 이를 갖고 더 많은 것을 탐색해 보세요.

## Pytest
테스트는 모든 개발 프로세스에서 중요한 부분이므로 비동기 기능을 위한 적절한 설정을 만들어 보겠다. 먼저 `pytest`, `pytest_asyncio` 및 `httpx` 패키지를 설치한다.

```bash
$ poetry add pytest
$ poetry add pytest_asyncio
$ poetry add httpx
```

프로젝트 루트 아래에 이 내용으로 `pytest.ini`를 만들어야 한다.

```
[pytest]
python_files = tests.py test_*.py *_tests.py
asyncio_mode=auto
```

또한 테스트를 실행할 별도의 데이터베이스가 필요하다. 현재 데이터베이스 heroes_db 근처에 heroes_db_test를 만들면 된다. 동일한 SQL 스크립트를 사용하되 이름만 약간 수정하고 `app/core/db.py` 파일에 몇 줄을 추가하면 된다.

```python
db_connection_str = settings.db_async_connection_str
if "pytest" in modules:
   db_connection_str = settings.db_async_test_connection_str


async_engine = create_async_engine(
   db_connection_str,
   echo=True,
   future=True
)
```

이 코드는 서비스가 pytest 환경에서 실행 중인지 식별하고 엔진 생성을 위한 테스트 데이터베이스 문자열을 선택한다.

또한 `.env` 파일과 `app/core/config.py`에서 `Settings` 객체를 확장하는 것을 잊지 마세요.

```python
DB_ASYNC_TEST_CONNECTION_STR="postgresql+asyncpg://hero:heroPass123@0.0.0.0:5436/heroes_db_tests"
```

```python
db_async_test_connection_str: str
```

위와 같이 추가한 후 적절한 비동기 테스트를 작성하는 데 도움이 되는 일련의 픽스처를 만들어야 한다. 모든 일반적인 픽스처는 `conftest.py`(`app/conftest.py`) 파일에 저장된다. 첫 번째 중요한 픽스처는 `event_loop`이며, 이 픽스처 없이는 비동기 테스트를 성공적으로 수행할 수 없다.

```python
# app/conftest.py

import asyncio
import json
import os
from typing import Generator

import pytest
import pytest_asyncio
from httpx import AsyncClient
from sqlalchemy.orm import sessionmaker
from sqlmodel import SQLModel
from sqlmodel.ext.asyncio.session import AsyncSession

from app import settings
from app.core.db import async_engine
from app.main import app


@pytest.fixture(scope="session")
def event_loop(request) -> Generator:  # noqa: indirect usage
   loop = asyncio.get_event_loop_policy().new_event_loop()
   yield loop
   loop.close()
```

또한 엔드포인트에 요청을 하려면 `async_client` 픽스처가 필요하다.

```python
@pytest_asyncio.fixture
async def async_client():
   async with AsyncClient(
           app=app,
           base_url=f"http://{settings.api_v1_prefix}"
   ) as client:
       yield client
```

또한 데이터베이스에서 생성된 객체를 확인하려면 데이터베이스 세션이 필요하다.

```python
@pytest_asyncio.fixture(scope="function")
async def async_session() -> AsyncSession:
   session = sessionmaker(
       async_engine, class_=AsyncSession, expire_on_commit=False
   )

   async with session() as s:
       async with async_engine.begin() as conn:
           await conn.run_sync(SQLModel.metadata.create_all)

       yield s

   async with async_engine.begin() as conn:
       await conn.run_sync(SQLModel.metadata.drop_all)

   await async_engine.dispose()
```

객체를 생성하고 응답을 확인하기 위한 테스트 데이터가 필요한 경우 이 픽스처를 사용한다.

```python
@pytest.fixture(scope="function")
def test_data() -> dict:
   path = os.getenv('PYTEST_CURRENT_TEST')
   path = os.path.join(*os.path.split(path)[:-1], "data", "data.json")

   if not os.path.exists(path):
       path = os.path.join("data", "data.json")

   with open(path, "r") as file:
       data = json.loads(file.read())

   return data
```

픽스처는 `tests.py`가 있는 디렉토리 아래의 데이터 폴더에 있는 `data.json` 파일에서 데이터를 검색한다.

## 테스트 작성
`app/heros` 디렉터리에 테스트 패키지를 만들고, `app/heros/data` 디렉터리에 `data.json`을 만들겠다.

```python
# app/heroes/tests/data/data.json

{
 "initial_data": {
   "hero": {
     "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
     "nickname": "TheLastManStands",
     "role": "mage"
   }
 },
 "case_create": {
   "payload": {
     "nickname": "TheLastManStands",
     "role": "mage"
   },
   "want": {
     "nickname": "TheLastManStands",
     "role": "mage"
   }
 },
 "case_get": {
   "want": {
     "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
     "nickname": "TheLastManStands",
     "role": "mage"
   }
 },
 "case_patch": {
   "payload": {
     "nickname": "TheLastManStands!",
     "role": "warrior"
   },
   "want": {
     "nickname": "TheLastManStands!",
     "role": "warrior"
   }
 },
 "case_delete": {
   "want": {
     "status": true,
     "message": "The hero hasn't been found!"
   }
 }
}
```

이제 테스트를 작성할 준비가 되었다. 첫 번째 테스트는 영웅 만들기이다.

```python
# app/heros/tests/tests.py

import pytest
from httpx import AsyncClient
from sqlalchemy import insert, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.heroes.models import Hero


@pytest.mark.asyncio
async def test_create_hero(
       async_client: AsyncClient,
       async_session: AsyncSession,
       test_data: dict
):
   payload = test_data["case_create"]["payload"]
   response = await async_client.post("/heroes", json=payload,)

   assert response.status_code == 201

   got = response.json()
   want = test_data["case_create"]["want"]

   for k, v in want.items():
       assert got[k] == v

   statement = select(Hero).where(Hero.uuid == got["uuid"])
   results = await async_session.execute(statement=statement)
   hero = results.scalar_one()

   for k, v in want.items():
       assert getattr(hero, k) == v
```

여기서는 `data.json` 파일에 제공된 테스트 데이터의 JSON 페이로드를 사용하여 `Hero` 객체를 생성하고, 데이터베이스에 쿼리를 수행하여 모든 필드가 일치하는지 확인한다. 아래 테스트에 대해서는 더 이상 언급할 필요가 없을 것 같다.

```python
# app/heros/tests/tests.py

...

@pytest.mark.asyncio
async def test_get_hero(
       async_client: AsyncClient,
       async_session: AsyncSession,
       test_data: dict
):
   hero_data = test_data["initial_data"]["hero"]
   statement = insert(Hero).values(hero_data)
   await async_session.execute(statement=statement)
   await async_session.commit()

   response = await async_client.get(f"/heroes/{hero_data['uuid']}")
   assert response.status_code == 200

   got = response.json()
   want = test_data["case_get"]["want"]

   for k, v in want.items():
       assert got[k] == v


@pytest.mark.asyncio
async def test_patch_hero(
       async_client: AsyncClient,
       async_session: AsyncSession,
       test_data: dict
):
   hero_data = test_data["initial_data"]["hero"]
   statement = insert(Hero).values(hero_data)
   await async_session.execute(statement=statement)
   await async_session.commit()

   payload = test_data["case_patch"]["payload"]
   response = await async_client.patch(
       f"/heroes/{hero_data['uuid']}",
       json=payload
   )
   assert response.status_code == 200

   got = response.json()
   want = test_data["case_patch"]["want"]

   for k, v in want.items():
       assert got[k] == v


@pytest.mark.asyncio
async def test_delete_hero(
       async_client: AsyncClient,
       async_session: AsyncSession,
       test_data: dict
):
   hero_data = test_data["initial_data"]["hero"]
   statement = insert(Hero).values(hero_data)
   await async_session.execute(statement=statement)
   await async_session.commit()

   response = await async_client.delete(f"/heroes/{hero_data['uuid']}")
   assert response.status_code == 200

   got = response.json()
   want = test_data["case_delete"]["want"]

   for k, v in want.items():
       assert got[k] == v

   statement = select(
       Hero
   ).where(
       Hero.uuid == hero_data["uuid"]
   )
   results = await async_session.execute(statement=statement)
   hero = results.scalar_one_or_none()

   assert hero is None
```

"pytest" 명령을 사용하여 모든 테스트를 실행할 수 있다.

## 요약
간단한 서비스 구현을 제공하는 FastAPI, SQLModel, Alembic 및 Pytest 패키지를 사용한 비동기 웹 서비스 설정에 대해 충분히 설명했다고 생각한다.

설정을 향상시킬 수 있는 몇 가지 솔루션을 더 들으면 기브다. 전체 프로젝트는 [GitHub 리포지토리](https://github.com/ETretyakov/hero-app)에서 확인할 수 있다.
